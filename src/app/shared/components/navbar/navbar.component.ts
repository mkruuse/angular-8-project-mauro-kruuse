import { Component, OnInit } from '@angular/core';
import { faUserCircle, faCoffee, faUserSecret } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  faUserCircle = faUserCircle;
  faUserSecret = faUserSecret;
  constructor() { }

  ngOnInit(): void {
  }

}
