import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginFormComponent } from '../features/components/login-form/login-form.component';
import { UserListComponent } from '../features/components/user-list/user-list.component';
import { UserProfileComponent } from '../features/components/user-profile/user-profile.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileGuard } from '../features/guards/profile.guard';
import { LoginGuard } from '../features/guards/login.guard';
import { UserFormComponent} from '../features/components/user-form/user-form.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'login', component: LoginFormComponent, canActivate:[LoginGuard]},
  { path: 'allusers', component: UserListComponent,canActivate: [LoginGuard]},
  /**{ path: 'profile', component: UserProfileComponent},*/
  { path: 'profile/:id', component: UserProfileComponent, canActivate: [ProfileGuard, LoginGuard]},
  { path: 'edit/:id', component: UserFormComponent},
  { path: '', pathMatch: 'full', redirectTo:'home'}, /* login*/
  /*{ path: '**', component: NotFoundComponent} -- ACÁ O EN EL PADRE????*/
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
