import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { LayoutComponent } from './shared/components/layout/layout.component';
/*import { UserFormComponent } from './features/components/user-form/user-form.component';*/
/*import { UserListComponent } from './features/components/user-list/user-list.component';*/
/*import { HomeComponent } from './pages/components/home/home.component';*/
import { NotFoundComponent } from './core/components/not-found/not-found.component';
/*import { LoginFormComponent } from './features/components/login-form/login-form.component';*/
/*import { UserProfileComponent } from './features/components/user-profile/user-profile.component';*/
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
/**Para que funciones STORE */
import * as fromUserReducer from './features/store/user.reducer';
import { StoreModule} from '@ngrx/store';
import { UsersEffects} from './features/store/user.effects';
import { EffectsModule} from '@ngrx/effects';
import { HttpClientModule} from '@angular/common/http';
import { ProfileGuardService } from './features/services/profile-guard.service';
import { LoginGuard} from './features/guards/login.guard';
import { ProfileGuard } from './features/guards/profile.guard';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LayoutComponent,
    /*UserFormComponent,
    UserListComponent,
    HomeComponent,*/
    /*LoginFormComponent,*/
    /*UserProfileComponent*/
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    StoreModule.forRoot({users: fromUserReducer.reducer}),
    EffectsModule.forRoot([UsersEffects]),
    HttpClientModule
  ],
  providers: [ProfileGuardService, LoginGuard, ProfileGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
