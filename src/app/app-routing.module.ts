import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent} from './core/components/not-found/not-found.component';
/*import { LoginFormComponent } from './features/components/login-form/login-form.component';*/
/*import { HomeComponent} from './pages/components/home/home.component';*/

const routes: Routes = [
  /*si pongo el loadChildren como segunda linea si me lleva al home pero no anda el active en el NAV*/ 
  
  /*{path: 'home', loadChildren: ()=> import('./pages/pages.module').then( m=> m.PagesModule)},*/
  /*otra prueba*/
  {
    path: '',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  },
  /*{path: 'home', component: HomeComponent},*/
  /*{path: 'login', component: LoginFormComponent},*/
  /*{path: '', pathMatch:'full', redirectTo:'home'},*/
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
