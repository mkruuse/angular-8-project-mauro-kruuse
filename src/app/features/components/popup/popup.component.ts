import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {
  
  /**Propiedades de entrada */
  @Input() visible :boolean;
  @Output() close : EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  cerrarPopup(){
    this.close.emit(false);
    /*this.visible = false;*/
    console.log('Emitiendo un false');
  }

}
