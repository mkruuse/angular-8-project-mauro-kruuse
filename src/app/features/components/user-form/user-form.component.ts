import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../models/user.model';
import { Store, State } from '@ngrx/store';
import { UserState } from '../../store/user.reducer';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  private userId: number;
  public userList :User[];
  public user :User;
  private cantReg:number;

  constructor(private route: ActivatedRoute,
              private store: Store<{ users: UserState }>,
              private router: Router ) {
    
    this.route.paramMap.subscribe(
      (rta) => {
        console.log('EDITAR USUARIO ' + rta.get('id'))
        this.userId = parseInt(rta.get('id'))
      }
    );
  }

  ngOnInit(): void {
    //console.log ('EDIT DATA '+ this.a.data.length);
    console.log ('on init EDIT DATA '); 
    this.store.subscribe(({ users }) => {
      console.log ('EDIT '+ users.data.length);
      this.cantReg = users.data.length;
      console.log(this.cantReg);
      this.userList = users.data;
    });
    console.log('DETALLE DE EDIT');
    this.userList.forEach(element => console.log(element));
    this.getUser();
  }

  getUser() {
    this.user = this.userList.find(usrid => usrid.id === this.userId);
  }

  cerrar() {
    this.router.navigate(['/allusers']);
  }

}
