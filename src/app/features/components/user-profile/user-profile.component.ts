import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';
import { User} from '../../models/user.model';

/** */
import { Store } from '@ngrx/store';
import { UserState } from '../../store/user.reducer';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  private userId:number;
  private cantReg:number;
  public userList :User[];
  public user :User;

  constructor(private route: ActivatedRoute, 
              private store: Store<{ users: UserState }>,
              private router: Router) {
    /**Otra forma de obtener el valor del parámetro sería
     * let val = +this.route.snapshot.paramMap.get('id');
     * el + al comienzo convierte el string a numero
     */
    this.route.paramMap.subscribe(
      (rta) => {console.log('ID DE USUARIO '+rta.get('id'))
      this.userId = parseInt(rta.get('id'))
      }
    )
   }

  ngOnInit(): void {
    this.store.subscribe(({ users }) => {
      //console.log ('profile '+ users.data.length);
      this.cantReg = users.data.length;
      //console.log(this.cantReg);
      this.userList = users.data;
    });
    //console.log('DETALLE DE VECTOR');
    //this.userList.forEach(element => console.log(element));
    this.getUser();
  }

  getUser(){
    //console.log('ENTRA AL GET USER');
    this.user = this.userList.find( usrid=> usrid.id === this.userId);
    //console.log ('USER ID PROFILE '+ this.user.id);
    //console.log('STREET '+this.user.address.street);
  }

  cerrar(){
    this.router.navigate(['/allusers']);
  }
}
