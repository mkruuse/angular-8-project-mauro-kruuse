import { Component, OnInit, HostBinding} from '@angular/core';
import { Router } from '@angular/router';
import { LoginService} from '../../services/login.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @HostBinding('class') clases = 'row'; /*El formulario esta en un Row para que sepa que bindear */

  admUsr: string; /*'test';*/
  admPass: string; /*'Ayi+2020';*/
  datosOk : boolean = true;

  constructor(
    private router: Router,
    public loginService: LoginService
  ) {
    this.admUsr = ''; 
    this.admPass = ''; 
  }

  ngOnInit(): void {
    
    if(this.loginService.sigueLogueado()){
      this.admUsr= this.loginService.usuario;
      this.admPass= this.loginService.pass;
    };
    console.log('ON INIT USR ' +this.admUsr);
  }

  login() {
    console.log('llega a la funcion LOGIN');
    console.log('USR ADMIN local '+this.admUsr);
    console.log('USR ADMIN lgdo '+this.loginService.sigueLogueado());

    //Login Autenticado
    if (!this.loginService.autenticar(this.admUsr, this.admPass)) {
      /**Me quedo en el form de login */
      console.log('usuario INCORRECTO.');
      this.datosOk = false;
    }
    else{
      console.log('contraseña ok? ' + this.loginService.sigueLogueado());
      /**Redirecciono a la ruta que corresponde*/
      this.admUsr = this.loginService.usuario;
      console.log('antes del route ' +this.admUsr);
      this.router.navigate(['/allusers']);
      this.datosOk =  true;
      console.log('despues del route ' +this.admUsr);
      console.log('despues del routet lgdo '+this.loginService.sigueLogueado());
      //this.reset();
    }
    
    //LOGIN ANTERIOR
    /* if (this.admUsr === 'test') {
      console.log('usuario correcto.');
      if (this.admPass ==='Ayi+2020') {
        console.log('contraseña ok');
        //**Redirecciono a la ruta que corresponde
        this.router.navigate(['/allusers']);
        this.datosOk =  true;
        this.reset(); // PARA VER SI NO QUEDAN LOS PARAMETROS PEGADOS EN EL URL
      } else {
        //**Me quedo en el form de login
        console.log('contraseña INVALIDA');
        this.datosOk = false;
        //this.reset();
      }
    } else {
      //**Me quedo en el form de login 
      console.log('usuario INCORRECTO.');
      this.datosOk = false;
      //this.reset();
    } */
  }

  logout(){
    this.reset();
    this.loginService.desloguear();
    this.router.navigate(['/home']);
    console.log('btn desloguear ' + this.loginService.sigueLogueado());
  }

  reset(){
    console.log('pasa por el reset');
    this.admUsr ='';
    this.admPass = '';
  }
}
