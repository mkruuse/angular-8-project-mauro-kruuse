import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { User} from '../../models/user.model';
import { Img} from '../../models/img.model';
import { Subscription } from 'rxjs';
import { Store, createSelector, State, UPDATE } from '@ngrx/store';
import { UserState } from '../../store/user.reducer';
import * as fromActionsUsers from '../../store/user.actions';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  /*@HostBinding('class') clases = 'row';*/ /*Esto hace que las tarjetas de los usr se vean al lado y no hacia abajo */

  userTest: User[]; /**Solo para la prueba inicial. Cargar con vla iniciales en el contructor*/

  public userList: User[] = [];
  public isLoading: boolean = true;
  public isCompleted: boolean;
  public cargaCompletada: boolean;
  private subscriptions: Subscription[] = [];
  public mostrarPopup = false; /**para el modal hecho a mano */
  public display: boolean; /**la usa el modal */
  private idUsrActual: number; /**guardo el id del usuario a borrar */
  public imgs: Img[] = [];

  public delUserById = createSelector(
    (state: UserState) => state.data,
    (users: User[], id: number) => users.splice(id, 1)
  );

  constructor(private store: Store<{ users: UserState }>, private router: Router) {
    this.isCompleted = false;
    this.cargarImagenes();
  }

  ngOnInit(): void {
    /**Cargar la lisa de usuarios*/
    /**No se si esto esta bien pero acá busco el store inicial para que 
     * no se vuelva a llamar al servicio nuevamente x que me pisa lo que
     * yo ya borre y/o edite. Es por el dispatch del FetchPending que a
     * mi parecer debe ser solo la primera vez
     */
    this.getStore();
    if (!this.cargaCompletada) {
      /**Solo si es la primera vez lo hago */
      this.store.dispatch(fromActionsUsers.FetchPending());
    }

    this.store.subscribe(({ users }) => {
      this.isLoading = users.pending;
      this.isCompleted = users.isFetchCompleted;
      if (users.data && users.data.length) {
        this.formatUsers(users.data);
        //this.loadImgs();
       }
    })
  }

  /**Funcion/servicio para formatear / reducir datos del objeto usuarios */
  formatUsers(users: any) {
    /**Quito columnas del vector (si fuera necesario)*/
    //console.log('ANTES DEL RETURN FORMATUSERS');

    this.userList = [];
    let unUser: User;
    let idAct: number = 0;
    for ( let item in users) {
      /**Asigno datos del servicio */
      //console.log(' ITEM + ', users[item].name);
      //this.userList. push(users[item]);
      /**Obtengo id de registro
       * lo quise hacer con map pero me devuelve el id de todos los registros
       * users.map(user => { return user.id} ); 1,2,3,4,....
       */
      
      /**Completo las URL de las imagenes*/
      unUser = {id:users[item].id ,
                name:users[item].name,
                username:users[item].username,
                email:users[item].email,
                phone: users[item].phone,
                website:users[item].website,
                address:{street:users[item].address.street,
                         suite:users[item].address.suite,
                         city:users[item].address.city,
                         zipcode:users[item].address.zipcode,
                         geo: { lat:users[item].id,
                                lng:this.imgs[item].url},},
                company:{catchPhrase:users[item].company.catchPhrase,
                         name:users[item].company.name,
                         bs:users[item].company.bs}}
      
      //this.userList[item] = unUser;
      this.userList.push(unUser);

      /**Actualizo url de las imagenes HARDCODE */
      //console.log(' OBJ ACTUAL ' + this.userList.find(id =>item).address.geo.lng);
      
      //console.log(' ITEM222 + ', this.userList[item]);
      
      /*this.userList.find(id=>idAct).address.geo.lat = idAct.toString();
      this.userList.find(id=>idAct).address.geo.lng = this.imgs[idAct].url;*/
    };
    
    return this.userList;
    /*CON ESTA SENTENCIA SOLO ME DEJA BORRAR UNA SOLA VEZ EN EL BTN BORRAR Y DESPUES SE
    *CLAVA ACA!
    return users.forEach(element => this.userList.push(element));*/

    /*CON ESTO SE PIERDEN LAS DESCRIPCIONES DE LAS CALLES Y DEMAS DATOS
      this.userList = users.map(user => {
      return {        
        id: user.id,
        name: user.name,
        username: user.username,
        email: user.email,
        phone: user.phone
      }
    })*/
  };

  /**Obtengo el estado inicial del Store */
  getStore() {
    this.store.subscribe(({ users }) => {
      this.cargaCompletada = users.isFetchCompleted;
    });

    if (this.cargaCompletada === null) { this.cargaCompletada = false }
  };

  renderUsers() {
    this.isLoading = false;
  };

  ngOnDestroy() {
    this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
  }

  /**Hacer funcion/servicio para obtener los usuarios */
  getUsers() {
    console.log('Estoy en getUsers');
  }

  deleteUser(id: number) { /*id:string */
    this.userList.splice(id, 1);
    this.closeModalB();
    console.log('Pasa por el delete user');
    const paylod = { users: this.userList };
    this.store.dispatch(fromActionsUsers.DeleteOneUser(paylod));
    /**despues de borrar hay q refrescar la vista this.getUsers(); */
  }

  /**ESTA ES LA FUNCIÓN QUE USA LA PANTALLA PARA BORRAR */
  deleteUserModal() {
    /**Borro el item del arreglo */
    this.userList.splice(this.idUsrActual, 1);
    console.log('DESPUES DE BORRAR UNO');
    this.userList.forEach(element => console.log(element));

    this.closeModalB();
    console.log('Pasa por el delete user Modal');
    /**Actualizo el state cuando borro un elemento*/
    const paylod = { users: this.userList };
    this.store.dispatch(fromActionsUsers.DeleteOneUser(paylod));
  }
  
  loadImgs(){

    /**AL FINAL NO LO USE. DEBERIA SER UNA FUNCIÓN GENERAL */

    /**Carga inicial de imagenes */
    
    /**Recorro todos los items */
    /* this.userList.forEach( item =>{
      console.log('ID2 RECUPERADO ' + item.id);
      console.log('OBJ2 ACTUAL ' + item.address.geo.lng);
      item.address.geo.lat = item.id.toString();
      item.address.geo.lng = this.imgs[item.id].url;
      console.log('POST OBJ2 ACTUAL ' + item.address.geo.lng);
    }); */

    let userListAux: User[] = [];
    
    for (let index = 0; index < this.userList.length; index++) {
      const element = this.userList[index];

      userListAux.push(element);

      console.log(' AUX ' + userListAux[index]);

      console.log('ID3 RECUPERADO ' + element.id);
      console.log('OBJ3 ACTUAL ' + element.address.geo.lng);

      userListAux[index]={id:element.id ,name:"MAURO KRUUSE",username:"",email:"",phone:"",website:"",
      address:{street:"",suite:"",city:"",zipcode:"",geo: { lat:"",lng:""},},
      company:{catchPhrase:"",name:"",bs:""}};
      /*this.userList[index].address.geo.lat = element.id.toString();
      userListAux[index].address.geo.lat = element.id.toString();*/
      /* Object.defineProperty (userListAux[index].name,"name",{
        value: "Mauro Kruuse",
        writable: true,
        configurable: true
      }); */
      console.log('XXXX'+ userListAux.map(value=>value.name)[index]);
      userListAux[index].name = userListAux.map(value=> value.name)[index]; //'Mauro Kruuse';
      //userListAux[index].name.replace('Leane',"mauro") ;
      /*this.userList[index].address.geo.lng = this.imgs[element.id].url;
      userListAux[index].address.geo.lng = this.imgs[element.id].url;*/
      console.log('POST OBJ3 ACTUAL ' + userListAux[index].name);
    }
    /**Actualizo el Store */
    const paylod = { users: this.userList };
    this.store.dispatch(fromActionsUsers.UpdateOneUser(paylod));
  };
  
  editUser(id: number) {
    console.log('Pasa por el edit');
    console.log('antes REFRESSSSSHHH');
    const paylod = { users: this.userList };
    this.store.dispatch(fromActionsUsers.UpdateOneUser(paylod));
    console.log('despues REFRESSSSSHHH');
    this.router.navigate(['/profile/' + id]);
  }

  /*Esto es para un modal propio*/
  muestraPopup() {
    this.mostrarPopup = true;
    console.log('mostrar' + this.mostrarPopup);
  }

  cerrarModal(evt) {
    this.mostrarPopup = evt;
    /**También se podría hardcodear el valor directamente
     * this.mostrarPopup = false ya que el emiter es el que
     * ejecutaría esta función.
     */
  }

  openModalB(id: number) {
    this.idUsrActual = id;
    this.display = true;
  }

  closeModalB() {
    this.display = false;
  }

  cargarImagenes(){
    
    this.imgs.push({id: 1, url : 'assets/images/M01.jpg'});
    this.imgs.push({id: 2, url : 'assets/images/H01.jpg'});
    this.imgs.push({id: 3, url : 'assets/images/M02.jpg'});
    this.imgs.push({id: 4, url : 'assets/images/M03.jpg'});
    this.imgs.push({id: 5, url : 'assets/images/M04.jpg'});
    this.imgs.push({id: 6, url : 'assets/images/H02.jpg'});
    this.imgs.push({id: 7, url : 'assets/images/H03.jpg'});
    this.imgs.push({id: 8, url : 'assets/images/H04.jpg'});
    this.imgs.push({id: 9, url : 'assets/images/M05.jpg'});
    this.imgs.push({id: 10, url : 'assets/images/M06.jpg'});
    this.imgs.push({id: 11, url : 'assets/images/H01.jpg'});
  }
}

/**this.userTest = [{
      id: 1, name: "Mauro Kruuse", username: "mkruuse", email: "mk@klk.com",
      address: {
        street: "Rivera Indarte", suite: "737", city: "Cordoba", zipcode: "5000",
        geo: { lat: "", lng: "" },
      },
      phone: "428-58696", website: "www.klk.com.ar",
      company: { name: "Kolektor", catchPhrase: "Yo me quedo en casa", bs: "no se hasta cuando" }
    },
    {id: 2, name: "Analia Suarez", username: "mkruuse", email: "mk@klk.com",
      address: {
        street: "Rivera Indarte", suite: "737", city: "Cordoba", zipcode: "5000",
        geo: { lat: "", lng: "" },
      },
      phone: "428-58696", website: "www.klk.com.ar",
      company: { name: "Kolektor", catchPhrase: "Yo me quedo en casa", bs: "no se hasta cuando" }
    },
    {id: 3, name: "Esteban Sanchez", username: "mkruuse", email: "mk@klk.com",
      address: {
        street: "Rivera Indarte", suite: "737", city: "Cordoba", zipcode: "5000",
        geo: { lat: "", lng: "" },
      },
      phone: "428-58696", website: "www.klk.com.ar",
      company: { name: "Kolektor", catchPhrase: "Yo me quedo en casa", bs: "no se hasta cuando" }
    },
    {id: 4, name: "Martín Gonzalez", username: "mkruuse", email: "mk@klk.com",
      address: {
        street: "Rivera Indarte", suite: "737", city: "Cordoba", zipcode: "5000",
        geo: { lat: "", lng: "" },
      },
      phone: "428-58696", website: "www.klk.com.ar",
      company: { name: "Kolektor", catchPhrase: "Yo me quedo en casa", bs: "no se hasta cuando" }
    }
  ] */