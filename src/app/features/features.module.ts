import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { UserFormComponent } from './components/user-form/user-form.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { FormsModule } from '@angular/forms';
import { PopupComponent } from './components/popup/popup.component'; /*para two way data binding de formularios*/


@NgModule({
  declarations: [LoginFormComponent, UserFormComponent, UserListComponent, UserProfileComponent, PopupComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  exports:[LoginFormComponent ,UserFormComponent, UserListComponent, UserProfileComponent, PopupComponent]
})
export class FeaturesModule { }
