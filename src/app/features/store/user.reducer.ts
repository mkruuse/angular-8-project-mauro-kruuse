import { createReducer, on, Action, Selector } from '@ngrx/store';
import { FetchPending, FetchFulfilled, FetchError, 
         ClearData, AddOneUser, DeleteOneUser, UpdateOneUser, LoadImagesUser } from './user.actions';

/**Estructura del estado a intercambiar */
export interface UserState {
    data: any;
    pending: boolean;
    error: boolean;
    isFetchCompleted: boolean;
}
/**Inicializacion del estado */
export const initialState: UserState = {
    data: null,
    pending: null,
    error: null,
    isFetchCompleted: null
}

/**Se crea el reducer */
const userReducer = createReducer(
    initialState,
    on(FetchPending, state => ({ ...state, pending: true, isFetchCompleted: false })),/**que significan los3 puntos? */
    on(FetchFulfilled, (state, { users }) => {
        return { ...state, data: users, pending: false, isFetchCompleted: true }
    }),
    on(FetchError, state => ({ ...state, error: true, pending: false })),
    on(ClearData, state => (initialState)),
    on(DeleteOneUser,(state, {users})=> {
        return { ...state, data: users}
    }),
    on(UpdateOneUser,(state, {users})=> {
        return { ...state, data: users}
    }),
    on(LoadImagesUser,(state, {users})=> {
        return { ...state, data: users}
    }),
    on(AddOneUser, (state, {users, user})=> { 
        return { ...state, date: users.push(user), pending: false, isFetchCompleted: true}
    })
);

export function reducer(state: UserState | undefined, action: Action) {
    return userReducer(state, action);
}