import { createAction, props } from '@ngrx/store';
import { User } from '../models/user.model';

export enum UserActionsType {
    FETCH_PENDING = '[USER ACTIONS: PENDING]',
    FETCH_FULFILLED = '[USER ACTIONS: FULFILLED]',
    FETCH_ERROR = '[USER ACTIONS: ERROR]',
    CLEAR_DATA = '[USER ACTIONS: CLEAR DATA]',
    DELETE_ONE = '[USER ACTIONS: DELONE]',
    ADD_ONE = '[USER ACTIONS: ADDONE]',
    UPD_ONE = '[USER ACTIONS: UPDONE]',
    LOD_IMG = '[USER ACTIONS: LOADIMG]'
}

export const FetchPending = createAction(
    '[USER ACTIONS: PENDING]'
);

export const FetchFulfilled = createAction(
    '[USER ACTIONS: FULFILLED]',
    props<{ users: User[] }>()
);

export const FetchError = createAction(
    '[USER ACTIONS: ERROR]',
    props<{ error: string }>()
);

export const ClearData = createAction(
    '[USER ACTIONS: CLEAR DATA]'
);

export const DeleteOneUser = createAction(
    '[USER ACTIONS: DELONE]',
    props< {users: User[] }>()
);

export const AddOneUser = createAction(
    '[USER ACTIONS: ADDONE]',
    props<{ users: User[]; user: User }>()
);
export const UpdateOneUser = createAction(
    '[USER ACTIONS: UPDONE]',
    props< {users: User[] }>()
);
export const LoadImagesUser = createAction(
    '[USER ACTIONS: LOADIMG]',
    props< {users: User[] }>()
);