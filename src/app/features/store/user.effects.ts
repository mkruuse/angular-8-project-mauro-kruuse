import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { UsersService } from '../services/users.service';

@Injectable()
export class UsersEffects {


    loadUsers$ = createEffect(() =>
        this.actions$.pipe(
            ofType('[USER ACTIONS: PENDING]'), /**No lo puedo traer del enum de user.actions??? */
            mergeMap(() => this.usersService.getAllUsers().pipe(
                map(users => {
                    //console.log('effect E ', users);
                    return { type: '[USER ACTIONS: FULFILLED]', users }
                }),
                catchError(() => of({ type: '[USER ACTIONS: ERROR]' }))
                )
            )
        ));

    constructor(private actions$: Actions, private usersService: UsersService) {
    }
};