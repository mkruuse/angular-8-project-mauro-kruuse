import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router, private loginService: LoginService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //console.log('GUARD LOGIN '+this.loginService.sigueLogueado());
    if (!this.loginService.sigueLogueado()) { //compruebo si el usuario esta logueado
      alert('Debe iniciar sesión para poder acceder');
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }

}
