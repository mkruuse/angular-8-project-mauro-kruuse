import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService} from '../services/login.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuard implements CanActivate {

  constructor(private router: Router, private loginService: LoginService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    /**Obtengo el id del perfil de la url
     * /profile/:id ---> 0 = profile, 1 = :id
     */
    //let id = +next.url[1].path;
    let id = +next.paramMap.get('id')
    if (isNaN(id) || id < 1) {// isNaN si no es un numero o es <1 se muestra el error
      alert("Identificador de perfil invalido!");
      if (this.loginService.sigueLogueado()){
        this.router.navigate(['/allusers']);
        return false;
      }
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }

}
