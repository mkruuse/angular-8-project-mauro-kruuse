import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProfileGuardService implements CanActivate{

  constructor(private router: Router) { }

  canActivate(route:ActivatedRouteSnapshot):boolean{
    /**Obtengo el id del profile de la url
     * /profile/:id ---> 0 = profile, 1 = :id
     */
    let id = +route.url[1].path;
    if(isNaN(id)||id <1){// isNaN si no es un numero o es <1 se muestra el error
      alert("Identificador de Perfil ivalido!");
      this.router.navigate(['/allusers']);
      return false;    
    }
    return true;
  }
}
