import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  /*Establecemos el endpoint */
  private endpoint = `${environment.EXT_URL_API}/users`;

  constructor(private http: HttpClient) { }

  /**Funciones del servicio*/
  getAllUsers() {
    return this.http.get(this.endpoint);
  }

  getUserById(id: number) {
    const url = `${this.endpoint}/${id}`;
  }
}
