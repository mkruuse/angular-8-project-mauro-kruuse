import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  usrLogueado = false;
  //usrLogueadoStrg :string= 'NO';
  usrLogueadoSStg :boolean;
  usuario = 'test';
  pass = 'Ayi+2020';
  
  constructor(
  ) { 
    //localStorage.setItem('usrLogueado','NO');
    //sessionStorage.setItem ('usrLogueado','false');
    //console.log('boolean false? '+ this.convertBoolean(sessionStorage.getItem ('usrLogueado')));
  }

  autenticar(usrName:string,usrPass:String){
    if (usrName === 'test' && usrPass === 'Ayi+2020'){
      this.usrLogueado = true;
      sessionStorage.setItem('usrLogueado','true');
      this.usrLogueadoSStg = this.convertBoolean(sessionStorage.getItem('usrLogueado'));
      //console.log('boolean true? '+this.convertBoolean(sessionStorage.getItem ('usrLogueado')));
      //console.log('pasa por el SERVICIO de logueo');
    } else {
      sessionStorage.setItem('usrLogueado','false');
      this.usrLogueadoSStg = this.convertBoolean(sessionStorage.getItem('usrLogueado'));
    }
    return this.usrLogueadoSStg;
    //return this.usrLogueado;

  }

  desloguear(){
    this.usrLogueado = false;
    sessionStorage.setItem('usrLogueado','false');
    this.usrLogueadoSStg = this.convertBoolean(sessionStorage.getItem('usrLogueado'));
    //console.log('pasa x el SERVICIO de deslogueo');
  }

  sigueLogueado(){
    //console.log(' SIGUE LOG puro + '+sessionStorage.getItem('usrLogueado'));
    //console.log(' SIGUE LOG bool + '+this.convertBoolean(sessionStorage.getItem('usrLogueado')))
    return this.convertBoolean(sessionStorage.getItem('usrLogueado'));
  }

  convertBoolean(isBoolean:string){
    if (isBoolean === 'true') {return true}
    else  {return false}
  }

}